TabArtstation
-------------------

Chrome extension to view latest artworks on `artstation.com` by its daily rss feed.

## Quick Start 

+ Install it from [Chrome Web Store](https://chrome.google.com/webstore/detail/tabartstation/emaecifjhbeeklconpljfnhhdkpedbgi)

Enjoy:)

## Change Log

+ 16.05.02: Use sequential index instead of the random index in rss items.


